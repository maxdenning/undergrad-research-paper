% --------------------------------------------------
% Data Set Creation
% --------------------------------------------------

% TODO: heavily edit the data collection protocol

\chapter{Data Collection} \label{chapter:Data}
The following section aims to document how data collection, the first objective of the project, was completed, and explain the outcomes. Prior to data collection taking place, ethical approval was sought from the University of Reading Computer Science Department. It was given, and the relevant documents can be found in Appendix \ref{appendix:ethics_documents}

%Appendices \ref{appendix:ethics_committee_info}, \ref{appendix:participant_info_sheet}, \ref{appendix:participant_consent_template}, and \ref{appendix:ethical_approval_confirmation}.


\section{Experimental Design}
\subsection{Aims}
The aim of this process was to create a dataset consisting of radar data, both raw ADC and processed samples, alongside video and metadata for ground-truth comparison. The data set was required to meet the standards defined in \ref{section:DataReqs}, and \ref{section:DataSetApproach}. Over 1000 samples were to be collected, distributed equally over the 5 defined class labels: No-Target, One Pedestrian, Two Pedestrians, Bicycle, and Car. These samples had to be collected over a range of participants, distances, velocities, and angles, in order to be as general and representative as possible.




%Data will be collected over a number of specifically defined scenes before being split and processed into individual samples. Over 1000 samples must be collected, distributed equally over the 5 defined class labels: No-Target, One Pedestrian, Two Pedestrians, Bicycle, and Car.

% Both the raw data and samples will be stored as output of this process. Samples must be stored as a matrix of values with shape $F \times R \times D$, where $F$ is the number of frames, $R$ is the number of range bins in each frame, and $D$ is the number of doppler bins in each frame. 




%\subsubsection{Definitions}
%\begin{itemize}
%	\item Scene - Represents the entire area captured by the radar and video camera. A scene may have any duration and contain any number of targets.
%	\item Sample - A subsection of a scene containing a target. It has a duration of 2 seconds and has one associated class label.
%\end{itemize}



\subsection{Method}
\subsubsection{Equipment}
Radar data was collected using a Texas Instruments (TI) IWR1642 single-chip 76-GHz to 81-GHz mmWave sensor and DCA1000EVM real-time data-capture adapter, loaned from AGD Systems Ltd. The configuration parameters chosen for the radar are shown in Table \ref{table:RadarConfig}.

\begin{table}
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabulary}{\textwidth}{ c  c }
		\toprule
		\textbf{Parameter} & \textbf{Value} \\
		\midrule
		&\\[-1em]
		Carrier Frequency & 76.5GHz (76 - 77GHz) \\
		Bandwidth & 0.912GHz \\
		Field of View & $\pm$60$^{\circ}$ \\
		Range Maximum (Resolution) & 49.99m (0.16m) \\
		Velocity Maximum (Resolution) & $\pm$10.98ms\textsuperscript{-1} (0.17ms\textsuperscript{-1}) \\
		Transmission Antennas & 1 \\
		Receiver Antennas & 4 \\
		Chirp loops per Frame & 128 \\
		ADC Samples per Chirp & 304 \\
	%	Sample Rate & 3798 kSamples/sec \\
		Frame Period & 0.04s (25 Hz) \\
		\bottomrule
	\end{tabulary}
	\caption{Radar configuration parameters}
	\label{table:RadarConfig}
\end{table}

The specification was designed to maximise the range and velocity measurement parameters. For example, a single transmission antenna was chosen because it allowed finer resolutions. This was due to a limitation on the size of the internal memory of the radar unit, which in turn limited the amount of data that could be outputted for each frame. Doubling the number of receiver channels, which would be the result of an additional transmission antenna, would have increased angle resolution. However, as angle is not being measured explicitly in this project, this is an acceptable cost for improved resolution on other measurements. A maximum range of 50m was also selected to increase range resolution. This is shorter than the range of the existing products found in \ref{section:ExistingProducts}, but it is still much further than what is present in current research and also fits the use-case of junctions or short stretches of road. The maximum velocity, equivalent to 24.6mph, is again much lower than that found in existing products, but it is still a realistic value that could be seen on residential roads. Additionally, collection was to take place on the University of Reading Whiteknights campus which has a speed limit of 20mph, so increasing the maximum velocity would have only reduced resolution without any gain in the variance of the dataset.

Video was collected simultaneously using a Microsoft LifeCam HD-3000 centred over the radar receiver antennas, to best match the field of view of the radar. Video capture (Table \ref{table:VideoConfig}) parameters were chosen to minimise the chance of accidentally capturing personally identifiable information (e.g. faces, number plates) from anyone, participant or otherwise.

\begin{table}
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabulary}{\textwidth}{ c c }
		\toprule
		\textbf{Parameter} & \textbf{Value} \\
		\midrule
		Resolution & 320$\times$240px \\
		Frame Period & 0.125s (8 Hz) \\
		Field of View & 60$^{\circ}$ \\
		\bottomrule
	\end{tabulary}
	\caption{Video configuration parameters}
	\label{table:VideoConfig}
\end{table}


\subsubsection{Data Collection Protocol}
To codify the variation of the dataset variables, a data collection protocol was produced, a copy of which can be found in Appendix \ref{appendix:data_collection_protocol}. This protocol defined a set of 20 scenarios (Table \ref{table:dataset_scenarios}) which detailed the direction, speed, and path of each participant, as well as each scene's duration, and the number of repeats required to complete the data set in a balanced way. Alphanumeric identifiers were assigned to each scenario, known in the metadata as scenario codes. Two substantially different locations were used for collection: a car park and an open playing field. This was to account for any effect the composition of the environment might have on background noise. The protocol also details that an elevation of 1.8m was chosen. A higher elevation may have been more representative of the use-case, however this was difficult to arrange due to the resources available at the time of data collection.


\begin{table}[h!]
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabulary}{\textwidth}{ c L c c }
		\toprule
		\textbf{Target Class} & \textbf{Description} & \textbf{\makecell{Approx.\\Velocity ($ms^{-1}$)}} & \textbf{Locations$^{\ast}$} \\
		\midrule
		No-Target & The scene will contain no participants and no intentionally moving targets. & N/A & \makecell{Playing field,\\car park 8} \\[0.4em]
		\hline
		\makecell{One\\Pedestrian} & Beginning at the radar, the participant will walk 40 metres away and back again at either 0$^{\circ}$ or 45$^{\circ}$ to the radar sensor. & 1.4 & Playing field \\[0.4em]
		\hline
		\makecell{Two\\Pedestrians} & Beginning at the radar, the participants will walk 40 metres away and back again at either 0$^{\circ}$ or 45$^{\circ}$ to the radar sensor. They will walk at the same distance, but separated horizontally by 0.5m. & 1.4 & \makecell{Playing field,\\car park 8} \\[0.4em]
		\hline
		Bicycle & Beginning at the radar, the participant will cycle 40m away and back again along the road. The radar will be at either 3$^{\circ}$ or 45$^{\circ}$ to the road. & 4.3 & Car park 8 \\[0.4em]
		\hline
		Car & Beginning at the radar, the participant will drive 40m away and back again along the road. The radar will be at either 3$^{\circ}$ or 45$^{\circ}$ to the road. & 8.9 & Car park 8 \\[0.4em]
		\bottomrule
	\end{tabulary}\\[0.5em]
	$^{\ast}$All locations were on the University of Reading Whiteknights campus
	\captionsetup{justification=centering,margin=1.5cm}
	\caption{High-level dataset scenario overview}
	\label{table:dataset_scenarios}
\end{table}


\subsection{Collection}
The configuration was stored as a JSON file, to ensure repeatability, and loaded onto the radar using TI's mmWaveStudio, an application for configuring and controlling their millimetre-wave products. To simplify the collection process, a data preview and capture application was produced (Figure \ref{fig:capture_tool_screenshot}). This application had two modes, preview and capture. Preview mode provided a low frame rate live preview of the video and processed radar data that was going to be captured, so the scene could be set up correctly. This was achieved by streaming raw data from the radar in the form of UDP packets, collecting them into raw frames, and then processing them into RDM frames for display using the first module from the processing pipeline. Capture mode disabled the preview outputs and facilitated the simultaneous capture of raw ADC data and video.

This was implemented by leveraging TI's mmWaveStudio, which presents a Lua environment from which high-level commands, such as begin or end capture, can be sent to the radar system. To interface with this environment Microsoft Message Queuing (MSMQ) was used, meaning radar data capture could be controlled externally. This approach was chosen over interfacing with the radar directly because it would have required using the low-level TI mmWave-SDK, a task which has already been done by mmWavestudio. Additionally, the low-level configuration command format was not well documented for this radar unit, and mmWaveStudio already presented a useful interface for this task. The application was written in Python 3.6, using PyQtGraph for the front-end, and video was captured in AVI format using OpenCV.

\begin{figure}
	\centering
	\includegraphics[scale=0.48]{res/capture_tool_example.png}
	\captionsetup{justification=centering,margin=1.5cm}
	\caption{Data capture tool showing a preview of video, RDM, and range-magnitude}
	\label{fig:capture_tool_screenshot}
\end{figure}


\subsection{Processing} \label{section:DataProcessing}
After collection, the raw ADC radar data was processed into samples using the batch processing pipeline, detailed in Section \ref{chapter:Implementation}. Raw ADC was processed into RDM frames of an entire scene. These scenes could contain any number of individual scenarios, and so the frame indices at which each scenario began and ended were manually recorded. Using this, scenes could then be split into their constituent scenarios. Targets were then extracted from the scenario RDMs to form samples and metadata (Figure \ref{fig:data_handling_flow}). Metadata was produced in the form of a single JSON file containing a map of sample IDs to sample metadata records. Each metadata record contained the unique sample ID, sample file path, classification, scenario code, source scene path, corresponding ground-truth video file path, time and location of collection, list of participants in the sample, and the centre range and velocity bin of the middle frame of the sample.

This process was automated for simplicity, and ease of use. Either a single step of the pipeline could be executed, or each separate module could be strung together to generate the entire sample set from raw data. Processing was performed in a batch-wise manner; for example all of the raw ADC data would be converted into RDM scenes before any scenes were then split into scenarios. A parameter could be specified so either the entirety of available input would be processed, or just one specific scene. This automation allowed for much more rapid testing, as changes could be made to the pipeline and then applied to a wide range of data very quickly. It also removed the need for human input, so a build could be triggered and then left to run for some time, which in turn lessened the chance of human error such as forgetting to process a certain set of input.

After processing, the data needed to be validated. This was done manually, with each sample being checked to see if it contained the target as described by its corresponding metadata.

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{res/data-handling-with-example.png}
	\caption{Data processing steps with examples}
	\label{fig:data_handling_flow}
\end{figure}

% talk about RadarConfig.py? and config as json

% Both the raw data and samples will be stored as output of this process. Samples must be stored as a matrix of values with shape $F \times R \times D$, where $F$ is the number of frames, $R$ is the number of range bins in each frame, and $D$ is the number of doppler bins in each frame. 



\section{Results}

The data collection protocol was executed, with each of the scenarios being collected as specified. The final dataset contained 2097 samples (104,850 frames), which was approximately double the intended size. Even with this increased size, the set remained roughly equally balanced (Figure \ref{fig:dataset_distribution}). This is a very positive change, as a larger data set should provide greater variance meaning more generalised models can be trained. Each class had at least two participants, with pedestrian and bicycle having three, coming to a total of 10 subjects. This is on par with existing literature (Table \ref{table:DataSetReview}). Most importantly it allows for separate training and test sets to be generated without there being participant contamination between them. Unfortunately, video was not captured for every sample due to some challenges faced while using the capture preview application. However, when this happened a single video was taken instead, to give a representative example of what the scene would have contained. This sufficed in most cases, as the video was only for ground-truth comparison.

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{res/results/dataset_participant_distribution.png}
	\caption{Distribution of classes and participants}
	\label{fig:dataset_distribution}
\end{figure}

\subsection{Data Exploration} \label{section:DataExploration}
Exploratory analysis was performed to better understand the shape of the data. Different tracking methods were compared as part of this project, and are discussed in detail in Section \ref{chapter:Implementation}. This meant different sample sets were produced, however those generated using a fixed-size region of interest CAMShift were selected for the purpose of this exploration.

All samples are a matrix of size $F\times{R}\times{D}$, where $F$ is the number of frames, $R$ is the number of range bins (x-axis), and $D$ is the number of Doppler bins (y-axis). As the radar was configured with a frame rate of 25Hz and samples are 2 seconds in duration, $F = 50$. During tracking and sample extraction, a window of size $32\times{32}$ was used, meaning $R = D = 32$. Summary statistics were generated over the entire sample set (Table \ref{table:data_summary_statistics}). They suggest that although the range of this data is quite wide, -61.87 to 24.20, the vast majority of the values are around -33, as the inter-quartile range is only 4.14. The median and mean are very similar, which suggests there are no extreme outliers and that the distribution could be unimodal. This is confirmed by the histogram (Figure \ref{fig:data_sample_distribution}), which shows that the sample set is normally distributed. A random selection of 100 samples was taken and their distribution was plotted (Figure\ref{fig:data_100_sample_distribution}), which shows the same is true of the individual samples too. This distribution is expected, as most of each frame will contain noise instead of signal. These results suggest that the samples are independent and identically distributed.

To visualise the variance of the dataset, principal component analysis (PCA) was used to reduce the data to two components. PCA is a method for reducing dimensionality that attempts to preserve maximum variance. This is done by projecting the data into a set of $n$ orthogonal axes which are sorted by the variance of the data on them, allowing those which describe the most variance to be selected and the others to be discarded with the least loss possible. This process was applied to a random subset of the samples (Figure \ref{fig:data_pca_samples}) and individual frames (Figure \ref{fig:data_pca_frames}), and resulted in over 80\% of the variance being discarded in both cases.

Both projections show that there are clear clusters for each of the classes, but they all have overlap which suggests that simple clustering methods, like K-means, would not be effective. In both projections, One Pedestrian and Two Pedestrians overlap, and Bicycles and Cars overlap, which suggests that a classification model will show the most inter-class confusion within these two pairs. The clusters in the sample projection are generally more distinct, which suggest that $\mu$-D signatures are better analysed as time series. One notable example is the cluster for No-Target observations, which has almost no overlap with any other clusters, unlike in the frames projection. This suggests that some frames in some samples contain almost no signal, which could be caused by distant targets having a much weaker reflected signal so they look like noise.  Overall, the clustering suggests that these classes could be distinguished by a trained model using higher dimensional representations.


\begin{table}
	\renewcommand{\arraystretch}{1.2}
	\centering
	\begin{tabulary}{\textwidth}{ c c c c c c c c }
		\toprule
		\textbf{Values} & \textbf{Min} & \textbf{Max} & \textbf{Mean} & \textbf{S. Dev} & \textbf{1\textsuperscript{st} Quartile} &  \textbf{Median} & \textbf{3\textsuperscript{rd} Quartile} \\
		\midrule
		106598400 & -61.87 & 24.20 & -32.48 & 5.12 & -35.13 & -33.04 & -30.99 \\
		\bottomrule
	\end{tabulary}
	\caption{Sample set summary statistics}
	\label{table:data_summary_statistics}
\end{table}

\begin{figure*}
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{res/results/dataset_sample_distribution.png}
		\caption[]
		\small{ }
		\label{fig:data_sample_distribution}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{res/results/dataset_100_sample_distribution.png}
		\caption[]
		\small{ }
		\label{fig:data_100_sample_distribution}
	\end{subfigure}
	\caption[]
	{\small Distribution of (a) the data set; (b) individual samples.}
\end{figure*}

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{res/results/dataset_samples_pca.png}
	\caption{PCA Projection of Subset of Samples}
	\label{fig:data_pca_samples}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{res/results/dataset_sample_frames_pca.png}
	\caption{PCA Projection of Subset of Sample Frames}
	\label{fig:data_pca_frames}
\end{figure}



%- outline final data set
%- statistics, quality, etc
%- example figures


\pagebreak
