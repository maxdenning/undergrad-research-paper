% --------------------------------------------------
% Radar Tech Overview
% --------------------------------------------------
% description of how radar works, what it is, etc
% lots of diagrams
% only 2 pages
% needs to be free of problem context (only what is included in the introduction)
% pretty much dont talk about the application space of radar

% description of FMCW radar
% diagram of infrastructure radar application

% what is radar

%TODO: referencing!

\chapter{Radar Detection Prerequisites} \label{chapter:Radar}

To better understand the contents of this document, a high-level understanding of radar is recommended. The aim of this section is to provide a brief introduction to radar technologies and processing methods that are relevant to topics discussed later.

\section{Measurement}
Radar is a system which uses radio waves to measure the properties of distant objects. Depending on the radar unit's specification, these properties can include range, velocity, angle, composition, or shape. Typically, such a system consists of a transmitter unit, transmitter antennas (Tx), a receiver unit, receiver antennas (Rx), and some form of embedded processing unit. To make a measurement the transmitter generates an electromagnetic pulse, in the radio or microwave domain, which propagates in a given direction. This pulse can be assumed to travel in a straight line at a constant speed ($3\times10^8ms^{-1}$); atmospheric conditions can cause this to vary, however the error is negligible. Any electrically conductive materials that lie along the path of the transmitted signal will cause scattering, and a small amount of energy will be reflected back to the receiving antenna. To calculate range, the round trip time of this pulse is measured and, given the assumption it travels linearly at constant speed, the relative distance of the reflecting object can be computed (Figure \ref{fig:radar_range}) \cite{RadarBasics_range}.

The relative velocity of objects can be measured using Doppler radar, which takes advantage of the Doppler effect (Figure \ref{fig:radar_doppler}). If an object is moving relative to the transmitter, each successive wave of the transmitted signal must travel a slightly different distance to reach the object before being reflected. This causes the reflected signal to have a higher or lower frequency than the transmitted signal, depending on the direction of travel. This difference, known as the Doppler frequency, can be detected by the receiver and allows for an accurate calculation of the object's radial velocity, taking into account the assumptions outlined earlier \cite{RadarBasics_dopplereffect}.

Information regarding the angle of approach of a reflecting object can only be determined if an array of offset receiver antennas are used. As the reflection from a target is diffuse, the returned signal can be detected in more than one location. At each of these locations the phase of the wave will be different, as it will have travelled a different distance from the reflecting object. Relative to a fixed set of known-offset receiver antennas, reflected waves from objects at different angles will have distinct phase-differences (Figure \ref{fig:radar_angle}). These phase-differences can then be computed into angle-of-approach estimations. The resolution of this measurement increases with the number of antennas available.

% TODO: clarify use of "diffuse"?

% ($v_{r}$) using
%\begin{equation}
%	|f_{D}|=\frac{2v_{r}f_{tx}}{c_{0}},
%\end{equation}
%where $f_{tx}$ is the original transmitted frequency, and $c_{0}$ is the speed of light [XXX].


% define electricaly leading + radial velocity


\begin{figure*}[h!t]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{res/radar_range_diagram.jpg}
		\caption[]
		{{\small Range measurement using round trip time \cite{RadarBasics_range_figure}}}
		\label{fig:radar_range}
	\end{subfigure}%
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{res/radar_doppler_diagram.png}
		\caption[]
		{{\small Velocity measurement using Doppler shift \cite{copradar_doppler_figure}}}
		\label{fig:radar_doppler}
	\end{subfigure}
	
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[scale=0.46]{res/radar_angle_diagram.png}
		\caption[]
		{{\small Angle estimation using phase difference \cite{wiki_angleestimation_figure}}}
		\label{fig:radar_angle}
	\end{subfigure}
	\caption[]
	{\small Examples of radar measurement methods}
\end{figure*}



\section{FMCW Radar}
Frequency-modulated continuous-wave (FMCW) radar is a specific subset of radar technology. Unlike the mono-pulse method described above, a continuous signal is emitted by the transmitter with a frequency that changes over time according to a pattern. Each repetition of this modulation is called a chirp, and it is by comparing these original and reflected chirps that measurements can be taken. A number of modulation patterns are used, but here the focus will be on linear sawtooth modulation. This is when each chirp linearly ramps frequency through the bandwidth over a given period, before resetting to zero (Figure \ref{fig:radar_sawtooth}).

Whereas in pulse radar the round-trip-time is measured directly for ranging, in FMCW the measurement is taken by comparing the received frequency with that of the original at a specific time and finding the shift in the time-axis. Velocity is found using the Doppler effect as described previously, seen here as a shift in the frequency-axis. This means that over consecutive chirps the phase difference can be measured to estimate an objects velocity. A single frame, containing data on all detected objects within view, is formed after performing a fixed number of chirp loops, within each of which a fixed number of range samples are taken on the ramping signal.
The sampling rate imposes a technical limitation on the maximum unambiguous detection range as they are proportionally related. The range resolution, which is how near two objects can be while remaining separable by the radar, depends on the bandwidth sweep of each chirp and is generally limited by legal restrictions on spectrum allocation. The maximum measurable velocity is inversely proportional to the idle time between each chirp loop which is physically limited by time taken to de-ramp the transmission frequency. Velocity resolution is inversely proportional to the frame period, which is limited by the desire for a higher frame rate. For angular estimation, limitations stem from the physical design of the antennas as the maximum value depends on the distance between each antenna and resolution on their length. Generally, it can be said that for each measurement property the second-hand effects of altering limiting factors for a maximum value will in turn proportionally increase the resolution value, thus producing a coarser measurement. The same is true in the reverse, where a finer resolution value would cause a lower maximum value. For reasons of brevity and scope, these details have been left out of this document.

\begin{figure}[h!]
	\centering
	\captionsetup{justification=centering,margin=1cm}
	\includegraphics[scale=0.3]{res/radar_sawtooth_diagram.png}
	\caption{Linear sawtooth modulation. Transmitted signal is shown in red, and received signal is shown in green. $\Delta{t}$ is the time shift used for ranging, and $f_{D}$ is the Doppler shift used for velocity. \cite{RadarBasics_sawtooth_figure}}
	\label{fig:radar_sawtooth}
\end{figure}

% [http://www.radartutorial.eu/02.basics/Frequency%20Modulated%20Continuous%20Wave%20Radar.en.html]



\section{Processing} \label{section:RadarProcessing}
A single frame output by the radar receiver is a matrix containing the ramp sample values converted from analogue to the digital domain (ADC), of size $S\times{C}\times{P}$ where $S$ is the number of ADC ramp samples per chirp, $C$ is the number of receiver channels (antennas), and $P$ is the number of chirps per frame. This is sometimes known as a radar data cube, and Fourier transforms are used to process its contents.

Fourier transforms (FTs) are a method of transforming a time domain signal into the frequency domain, such that the waveform is decomposed into its component frequencies. To detect an object it must be able to be resolved in at least one dimension, range, velocity, or angle, and FTs enable this by allowing components to be picked out from the ADC data and resolved. First, a transform is applied along the ADC samples to resolve objects in range. Next, another transform can be applied along the chirps to resolve objects in velocity. Additionally, a transform can be applied across the receiver channels to resolve objects by angle. This forms the data into bins for each measurement type, where each bin represents the measurement resolution and contains the reflected signal strength at that measurement value. For example, with a maximum range of 5 metres and a resolution of 0.5 metres, there would be 10 range bins and the strength of the reflected signal from any objects $n$ metres away would be contained within the $n^{th}$ bin.


\section{Visualisation} \label{section:RadarVisualisation}
There are a variety of common methods for visualising processed radar data, the most straight-forward of which are range or velocity graphs (Figure \ref{fig:radar_graph_range}). Each graph represents a single frame and plots the reflected signal's magnitude against range or Doppler bins. Peaks in magnitude indicate that a reflecting object is at a given range or velocity.

Range-Doppler maps (RDMs) project the data in three dimensions: range bin, Doppler bin, and magnitude (Figure \ref{fig:radar_graph_rdm}). Time can then be represented by using a series of frames to form a sort of video. A band of high-power noise is usually present along the centre velocity bin (i.e. velocities of $0ms^{-1}$), which is caused by reflections from still objects.

Spectrograms represent time more directly by plotting Doppler frequency against time (Figure \ref{fig:radar_graph_spectrogram}). As discussed earlier, Doppler frequency is related to velocity. The plots are produced using a different approach to that described above; a short time Fourier transform (STFT) is performed which decomposes the signal using a time windowing function. These time windows are then concatenated into a complete representation.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{res/radar_graph_rangepower.png}
	\caption{Range-Magnitude visualisation}
	\label{fig:radar_graph_range}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{res/radar_graph_rdm.png}
	\caption{Range-Doppler map of a car at close range, travelling at moderate speed}
	\label{fig:radar_graph_rdm}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.65]{res/radar_graph_spectrogram_fig.png}
	\caption{Doppler spectrogram of a car \cite{Angelov18}}
	\label{fig:radar_graph_spectrogram}
\end{figure}

\pagebreak
