% --------------------------------------------------
% Literature Review
% --------------------------------------------------

% The literature review is an essential component of your project report. You should discuss the existing literature that is relevant to your project with full and proper referencing. You should aim to refer to a range of material including academic papers, text books, articles and existing product descriptions. It should be clear to the reader why the literature you identify is relevant and how you have incorporated the learnings from your review into your project. For example, you may have made a number of project decisions based on your review of the literature and these decisions should be described. The literature review should also lead to the creation of a number of possible solutions to your problem articulation and technical specification.

\chapter{Literature Review} \label{chapter:Lit}

%- discuss existing material, refer to a range - papers, text books, articles, and existing products (ask AGD)
%- make it clear to the reader why the lit identified is relevant, and how the learnings have been incorporated into the product
%- describe decisions made from reviewing lit
%- lit review should lead to creation of a number of possible solutions to your problem articulation  and tech spec
%
%!!! - include search for data set / justification for making my own
%


% sections:
%	radar vs imaging / how radar works?
%	search for existing data sets
%	tracking algos
%	approaches from similar studies
%	automotive vs infrastructure (popularity)
%	machine learning approaches
%	dive into machine learning specifics
%	non-ML methods (? probably not relevant)
%	tracking metrics
%	classification metrics
% 	intrastructure limitations/specifications etc
%	compare data set sizes used in similar studies

%%%%%%%%%%
% preamble
% define how objectives can be broken down into topcis

% topcis:
% existing products / systems (actual irl deployed systems)
% sensor choice (why radar vs optical, then how has existing lit configured their radar) (highlight diff between automotive and infrastructure?)
% existing data sets (look at sizes used in other studies)
% tracking algos + metrics
% preprocessing techniques (denoising)
% lit from related subject - e.g. human gait recognition
% automotive classification approaches in existing lit
% other classification approaches (video)


% preamble...

\section{Existing Products} \label{section:ExistingProducts}
Within the road infrastructure space, products integrating classification technology are becoming more common, but it is still a growing area of development. For these products, finding the methods that have been implemented poses a challenge, as they are usually kept private to remain competitive. Classical statistical classification methods for radar have existed for some time, such as using velocity thresholding and Fischer linear discriminant analysis on Doppler spectra \cite{Stove02}, and so it could be expected that these methods have been used in existing products. Similarly, finding useful metrics on their performance is also difficult. However, a 2018 Department for Transport (DfT) review of methods for gathering traffic statistics (such as count and classification) found that none provided the accuracy of vehicle classification required \cite{DfT18}. Methods in this review included radar and automatic video recognition, and were compared against existing methods, such as manual counting and the use of automatic traffic counters embedded in the road surface. The classifications they aimed to identify were bicycles, 2-wheeled motor vehicles, cars, buses, and a range of large goods vehicles. These results suggest that the products currently deployed on the UK's roads do not perform to a high standard, and as such further research is needed.

Although it is difficult to determine specific detail about existing products, a review of high-level specification can still take place. SmartMicro have released a product line of arterial management radar, UMRR-XX \cite{SmartMicro}, which advertise real-time detection of 5 classes: undefined, pedestrian, bicycle, single car, and truck. Performance is defined as "precise" but no metrics were given. Items in this product line are documented as using FMCW radar, operating at 24.5GHz, and having a maximum detection range for a passenger car of 230m. PWS sell the SR4, a traffic counting and classification product \cite{PWS} which is aimed more at providing statistics for studies, like the aforementioned study conducted by the DfT, compared to SmartMicro's products which are for traffic management. The SR4 requires the data to be downloaded and input to a separate desktop application for processing classifications (the classes being motorcycle, car, truck, and large truck). This suggests there is a demand for both real-time and deferred processing approaches, and that there is an interest in classifying a wide a range of traffic, with an emphasis on larger vehicles.



\section{Existing Data Sets} \label{section:ExistingDataSets}
% look for existing data sets
% then talk about what specifications were used in existing research and in deployed systems

As defined in Section \ref{chapter:Problem}, a data set is required as part of this solution. However, after review, no public datasets meeting the requirements outlined in \ref{section:DataReqs} could be found, or indeed any at all containing raw radar data collected in a road traffic infrastructure scenario. In contrast, numerous data sets exist in the automotive space for training autonomous vehicles \cite{Yin17}. One such dataset is nuScenes \cite{nuscenes19}, which is a multimodal set containing a mix of video and radar data, amongst others. NuScenes contains 1000 20-second scenes with 23 object classes. The FMCW radar operates at 77GHz and is configured to have a maximum range of 250m, a velocity resolution of $\pm0.28ms^{-1}$, and to capture 13 frames per second. The classification labels include animal, adult, child, bicycle, motorcycle, car, truck, and two types of bus, amongst others.

To better decide on a dataset approach, it is useful to review what is used in current research. It is common for studies using radar, for a variety of tasks, to create their own data set. A subset of such research was reviewed and what follows is a brief outline of what was found (Table \ref{table:DataSetReview}).

\begin{table}[h!]
\begin{small}
	\renewcommand{\arraystretch}{1.4}
	\centering
	\begin{tabulary}{\textwidth}{ L L L }
		\toprule
		\textbf{Subject} & \textbf{Radar Parameters} & \textbf{Data Set Parameters} \\
		\midrule
		Classification of moving targets using automotive radar \cite{Angelov18} & 76.5GHz FMCW, 1$\times$Tx 4$\times$Rx, 0.7m elevation & 4 classes (pedestrian, two pedestrians, bicycle, car), 90 minutes of data processed into 186 2$s$ samples, 0$^{\circ}$ angle of approach \\
		\midrule
		DNN initialisation methods for $\mu$-D classification \cite{Seyfioglu17} & 4GHz CW, 1m elevation, 0-5m range & 12 classes (human locomotion/activity), 1007 4$s$ samples, 11 participants \\
		\midrule
		Human activity classification based on $\mu$-D signatures \cite{Kim16} & 2.4GHz & 7 classes (human activity), 1008 3$s$ samples \\
		\midrule
		Dynamic continuous hand gesture recognition \cite{Zhang18} & 24GHz FMCW, 1$\times$Tx 2$\times$Rx, 1.5m range & 8 classes (hand gestures), 3200 1.5$s$ samples, 4 participants \\
		\midrule
		Adversarial Denoising of $\mu$-D signatures \cite{Armanious18} & 25GHz CW, 12$ms^{-1} V_{max}$ & Human gait, 18520 images \\
		\midrule
		Multiple joint-variable domain recognition of human motion \cite{Jokanovic17} & 24GHz FMCW, 0.075m $R_{res}$, 4m range & 4 classes (gross human motion), 96 4$s$ samples, $0^{\circ}-45^{\circ}$ angle, 3 participants \\
		\bottomrule
	\end{tabulary}
\end{small}
	\caption{Datasets used in micro-Doppler studies}
	\label{table:DataSetReview}
\end{table}

Dataset parameters tend to be reported with varying degrees of detail, making it difficult to compare them directly, but one clear conclusion is that the majority have over 1000 samples and use samples of over 1$s$ in duration. Work presented in \cite{Seyfioglu17} suggests that classification models for $\mu$-D trained on fewer than 650 samples are outperformed by transfer learning approaches from the optical domain. This is a very low number of samples, which suggests that the features learned for optical tasks are sufficiently different from those needed for radar classification tasks that transfer learning is not a useful approach for this project. It has also been found that when using spectrogram data a 2.5$s$ duration for each sample provided the best classification accuracy \cite{Kim16}. However, the results were not impacted dramatically even when this window was halved.


\section{Target Detection \& Tracking} \label{section:LitTargetTracking}
% review kalman, cfar, camshift
% review mota and short-term single target tracking metrics

As outlined in the objectives, part of this process will require the processing of raw data into individual samples. As such, target detection and tracking will be needed to extract targets from a scene. Detection methods from both the radar and image analysis domains have been considered.

\subsubsection{Constant False-Alarm Rate}
Constant False-Alarm Rate (CFAR) is an adaptive algorithm for peak detection and noise reduction that is widely used in radar processing \cite{Angelov18}\cite{Armanious18}. There are a number of CFAR approaches, the most common of which is cell averaging CFAR (CA-CFAR). A vector is taken as input and is iterated over as follows. A position is chosen as the cell under test (CUT) and its value must be greater than the estimated noise threshold $T_{ca}$ to pass as a peak. This threshold is estimated using a reference window of $N$ adjacent cells, although those immediately adjacent are usually used as "guard cells" and are ignored so as not to corrupt the noise estimate. The threshold is calculated as

\begin{equation}
T_{ca}=\alpha_{ca}\sigma_{i},
\end{equation}
\begin{equation}
\alpha_{ca}=N(P_{fa}^{-1/N}-1),
\end{equation}
\begin{equation}
\sigma_{i}=\frac{1}{N}\sum_{n=1}^{N}x_{n},
\end{equation}

where $\alpha_{ca}$ is the CA-CFAR constant, $\sigma_{i}$  is the mean interference power (noise) of the reference window $x$ centred at position $i$, and $P_{fa}$ is the false alarm rate. The false alarm rate is a measure of how often, in the absence of a target, a source of interference produces a measured value that exceeds the detection threshold \cite{Richards10}. If the CUT exceeds the threshold, its index is stored and the process is repeated on the next position. CA-CFAR operates on two assumptions: firstly that the interference is independently and identically distributed, and secondly that the reference window does not contain signal return from other targets which could bias the threshold estimate. Environments that conform to these assumptions are known as homogeneous, and might only contain a single target. The effect of the second assumption being violated is called target masking, as it erroneously increases the threshold value such that valid targets are masked. Selecting a larger $N$ or using more guard cells can remedy this behaviour to some extent, but it increases complexity and the chance of capturing another target in the window. Enhanced methods such as ordered-statistics CFAR (OS-CFAR) are designed to avoid this target masking effect. OS-CFAR rank orders the $N$ samples in the reference window and selects the $k^{th}$ sample as the CFAR statistic, thus being able to reject $N - k$ nearby interfering targets \cite{Richards10}.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.45]{res/cfar_methods_fig.png}
	\captionsetup{justification=centering,margin=1.5cm}
	\caption{Comparison of CA-CFAR and OS-CFAR. CA-CFAR misses a target due to target masking. \cite{Richards10} (Figure 16-20)}
	\label{fig:cfar_methods}
\end{figure}

\subsubsection{Kalman Filters}
CFAR peak detection itself can be used for rudimentary target tracking, or it can be integrated with more complex algorithms like Kalman filters, as seen in \cite{Angelov18}. Kalman filters are a stochastic predictor-corrector algorithm, which predicts state given input containing interference. The predictor uses prior state to generate the next time step, which is then updated by the corrector using an average of new measurements weighted by measurement certainty \cite{Richards10}. It is useful for multi-target tracking, as the predictive step can be used to differentiate between new detections and assign them to the correct track.

\subsubsection{CAMShift}
Kalman filters are used across many domains, including computer vision. Another tracking algorithm in this domain is CAMShift, which is a type of continually adaptive mean-shift algorithm \cite{Bradski2008}. Mean-shift uses kernel density estimation to find the gradient of a data distribution with the goal of finding local minima, where the gradient is 0. To so do, an initial fixed-size window is placed on the input, which is a 2-dimensional histogram measuring the density of points in a given space. The centroid of the windowed points are found and the window is moved to that position. This process is repeated until the position becomes stable. If the input were video, for example, then the next frame's density distribution would be presented to the algorithm with the previous final window acting as the new initial window, allowing frame-by-frame tracking. Windowing is used to limit what input is analysed so outliers in the overall distribution do not affect convergence, making the process more robust. CAMShift builds upon mean-shift, as after convergence it automatically adjusts the window size and orientation by finding the best fitting ellipse over the distribution. This allows for more precise tracking of targets that alter in size over time, like someone walking towards the camera. Much like Kalman filters, both of these algorithms require the object to be picked out of the scene before tracking can take place. However, this only needs to take place once, instead of at every measurement input. As well as this, they are best for single-target tracking, as they do not take previous trajectory into account which means crossing target paths can be easily confused.


\subsubsection{Validation Metrics}
No matter the method of tracking, it will require validation via comparison to ground-truth tracks. Given the narrow application it is possible to limit the types of metrics required. For example short-term methods can be used for single-target tracking, which assume the target does not completely disappear from the scene at any point. A review of such methods is presented in \cite{Cehovin15}.

While there are no predominant methods, the two most popular are centre error and region overlap. Centre error measures the difference between predicted, or hypothesis, windows $H$ and ground-truth windows $G$ centres, and because of this it is very simple to implement. It can be summarised as the root-mean-squared sum of errors,
\begin{equation}
CE_{RMSE}(G,H) = \sqrt{\frac{1}{N}\sum_{t=1}^{N}||x^{G}_{t}-x^{H}_{t}||^2},
\end{equation}
where $N$ is the number of windows, $t$ is the time step of the track, $x$ is a vector containing centre coordinates. However, this measure does not take window size into account and so is not individually useful for identifying failed tracks. Measures based on region overlap use pixel-wise intersect and union values to generate more representative metrics, so they account for both position and size. They also do not generate arbitrarily large errors, like centre error, as they tend to zero as the hypothesis drifts. Region overlap is defined as
\begin{equation}
RO(G,H)_{t} = \frac{R^{G}_{t} \cap R^{H}_{t}}{R^{G}_{t} \cup R^{H}_{t}} = \frac{TP}{TP + FN + FP},
\end{equation}
where $R$ is the number of pixels in a region and $t$ is the time step of the track. As it is possible to define in terms of confusion values (true/false-positive/negative) other useful scores like F-measure and precision can be generated.

This can be used in conjunction with a threshold value to determine if enough of an overlap has occurred. From this, the number of successfully tracked frames can be calculated, and summarised into a percentage of correctly tracked frames. Track length can also be generated, which records the number of frames before the first failure. This can be useful for finding specific scenarios in which the algorithm struggles.

For multi-target tracking, a popular approach is to use multi-object tracking precision (MOTP) and accuracy (MOTA) \cite{Milan16}. These metrics are used as part of the MOTChallenge benchmark \cite{MOTChallenge16}, which provides large annotated challenging multi-target pedestrian video datasets for tracking algorithm comparison. To compare success, hypotheses are put through a threshold, as described earlier, and from this categorised as confusion values, for example a TP would be a valid hypothesis window. Windows are also assigned to a persistent track ID, so the metrics can take into account how well the tracker can maintain tracks between frames, as a common problem is tracks being swapped when targets travel near each other. MOTP and MOTA are defined as

\begin{equation}
MOTP = \frac{\Sigma_{t,i}d_{t,i}}{\Sigma_{t}c_{t}},
\end{equation}
\begin{equation}
MOTA = 1 - \frac{\Sigma_{t}(FN_{t} + FP_{t} + IDSW_{t})}{\Sigma_{t}GT_{t}},
\end{equation}

where $t$ is the time step, $i$ is the target ID, $c$ is the number of matches in a frame, $d$ is the bounding box overlap of a target with its ground-truth, $GT$ is the number of true objects in frame, and $IDSW$ is the number of identity switches. MOTP measures the localisation precision of a tracker, as it is the average overlap between all valid hypotheses and their ground-truths. MOTA combines three sources of error, and as such is a good way of evaluating overall performance.

The validation methods reviewed here are primarily concerned with optical video tracking validation, but it is reasonable to assume they will transfer well to the radar domain, depending on the format of the data used.



\section{Machine Learning Methods} \label{section:ClassificationMethodsReview}
It was found that research concerning classification using radar in a traffic management scenario is somewhat sparse. However, automotive or human activity classification applications are much more popular. Automotive radar research is likely experiencing increased interest due to the recent boom in semi-autonomous vehicle development. Fortunately, the methods presented by these related topics are very applicable to the scenario this project aims to investigate. This is especially true of automotive research due to the end goals being broadly similar. What follows is the significant findings of a review of the current state of radar machine learning research.


\subsection{Data Format}
Several methods of processed radar data visualisation were outlined in \ref{section:RadarVisualisation}, range-magnitude plots, RMDs, and spectrograms. Range-magnitude plots provide a limited view of the data, and to resolve a target it must be separable in at least one domain, range, velocity, or angle. These plots show only a single dimension, so if multiple objects fall within the same bin, they cannot be easily identified. This potentially makes the format less useful. No research was found which used this data format as an input to a machine learning algorithm. Range-Doppler maps allow easier target resolution as the data is projected into three dimensions, which gives more information at the cost of increased dimensionality. This is especially true if time is taken into account, as multiple frames must be used. Spectrograms manage to incorporate time whilst having a comparatively significant reduction in dimensionality. However, it does not contain any range information.

It was found that spectrograms are the primary form of representation in existing research \cite{Angelov18}\cite{Kim16}\cite{Zhang18}, with little deviation, although RDMs were found to be used in at least one machine learning context \cite{Jokanovic17}. This imbalance could be due to range not being considered as useful a measurement as velocity, since the relative distance of an object does not give any information about its classification. Meanwhile, it has been shown that velocity thresholding alongside Fischer linear discriminant analysis can yield successful classification \cite{Stove02}. Therefore, the cost of no range information for reduced dimensionality may be worth it. However, the range information given by RDMs can include an object's physical size, which likely can be used for classification, meaning RDMs may have untapped potential.


\subsection{Micro-Doppler Analysis} \label{section:uDAnalysis}
Micro-Doppler analysis is the name given to research focusing on analysing Doppler frequency features generated with millimetre-wave radar (30 - 300GHz). Research has been conducted at 77GHz showing that moving pedestrians and cyclists produce significantly different $\mu$-D signatures \cite{Belgiovane17}. When measuring pedestrians, elevation was varied between 0.5m and 1m, which is more suitable for automotive applications. While travelling at 0$^{\circ}$ it was found that at 0.5m, torso, leg, and arm movements are clearly visible (Figure \ref{fig:uD_ped_signature_0deg}). However, this was much less pronounced at 1m with the majority of reflection coming from the torso. This was likely in part due to the radar having a field of view of only 14$^{\circ}$, which is very narrow. When angled at 90$^{\circ}$, these individual signatures were not present (Figure \ref{fig:ud_ped_signature_90deg}). For bicycles, it was found that leg and wheel oscillations presented distinct temporal behaviour compared to pedestrian signatures. However, once again, both were most visible at 0$^{\circ}$, and were diminished as angle increased. This shows that at close range, these two classes are very distinct and potentially different enough that this will hold true at further distances, provided resolution is fine enough. Additionally, the findings stress the importance of angle variance for training a generalised model on radar data.



\begin{figure*}
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{res/uD_ped_signature_00_fig.png}
		\caption[]
		{{\small Torso, arm, and leg oscillation is clearly visible}}
		\label{fig:uD_ped_signature_0deg}
	\end{subfigure}%
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{res/uD_ped_signature_90_fig.png}
		\caption[]
		{{\small Individual limb oscillation can no longer be seen}}
		\label{fig:ud_ped_signature_90deg}
	\end{subfigure}

	\caption[]
	{\small  $\mu$-D signatures of a pedestrian at (a) 0$^{\circ}$, (b) 90$^{\circ}$ \cite{Belgiovane17} (Figures 5, 6)}
\end{figure*}






\subsection{Convolutional Networks}
As it has been shown that $\mu$-D analysis has the potential to produce distinct target signatures, it has been combined with the growing field of machine learning through artificial neural networks. The majority of material in the automotive and medical radar domain focus on the use of various types of convolutional neural networks for feature extraction. This aligns with the popularity of spectrograms, as convolutional approaches have become the de-facto standard for image recognition tasks \cite{Szegedy15}\cite{He15}, and spectrograms are a visualisation of a target's velocity.

A recent review of classification approaches for automotive data focussed almost entirely on convolutional networks and found promising results \cite{Angelov18}. One of the aims of this research was to look at low latency processing and classification, which makes it a particularly useful example for this project as it presents a realistic view of deployable network architectures. Three networks were compared, a downscaled VGG-like \cite{Simonyan14} network with two units of convolutional and max-pooling layers before densely connected layers; a residual convolutional network based on ReNet50 \cite{He15}, which uses skip layers to avoid vanishing gradients and speed up learning; and a recurrent network which uses a convolutional layer to extract features which are then input to a long short-term memory layer (LSTM). To train the networks, data was collected over 4 target classes (pedestrians, two pedestrians, bicycles, and cars), producing 0.5 second and 2 second long spectrograms (Table \ref{table:DataSetReview}). The networks were compared using 3-class problems: car-person-bicycle, and car-person-2people. When training the LSTM network, each sample was split into sequences of 0.25 seconds. Generally, the result of each test was approximately 80\% accuracy, except for the LSTM network trained on 2-second samples which had an accuracy of 93\%. This suggest that longer samples significantly improve the ability of LSTM networks to learn this data, which is expected as LSTMs are a technique for analysing time series. Overall, the results show that convolutional layers can be very successful when applied to the radar domain.

However, the accuracy of some of the results produced in this paper can be contested. The models were trained with very low sample support, as only 186 sample were generated in total. The sample set they collected was very imbalanced, for example the pedestrian and car classes had 60 samples, whereas the bicycle class had only 22. During training this was accounted for by downsampling the larger classes to the size of the smallest, but this only compounds the problem of low sample support. The results were generated using 5-fold cross-validation, with the subsets being randomly selected. The number of participants was not recorded in the paper, but randomly sampling across the entire data set can cause contamination between the training and test set, if participants are present in both. This has the potential to artificially improve the accuracy results of the model. These issues do call into question how truly general their models were, but overall it does not necessarily mean their network approaches were invalid, as they are based on other research. 


% for each of these, give aim, method, and findings. give critical review and be clear why it is significant and worth talking aboutANN





\subsubsection{Video Classification}
Micro-Doppler analysis is be inherently about extracting temporal features of target signatures over time. As mentioned, RDMs present this information through sequential frames, much like a video. As such, approaches for classifying video in the optical domain may transfer well to the radar domain.

State of the art video classification approaches tend to use variations of two stream architectures, a technique pioneered by \cite{Simonyan14video} and subsequently refined over the last few years \cite{Feichtenhofer16} \cite{Wang16} \cite{Carreira17}. Two stream architecture essentially splits the problem into spatial and temporal domains, which are analysed by each of the streams. The spatial stream takes a single frame from the video sample, and usually uses convolutional layers to extract features in a similar way to most standard image classification approaches. The temporal stream differs in that dense optical flow vectors are computed from the video sample and these are used as 3-dimensional input to the second stream. Optical flow looks at sequential video frames to calculate the vector of motion for pixels and objects within the image \cite{Bradski2008}. It is used here in an attempt to extract meaningful information about the actions of objects in the scene, because it is the motion itself which often contains the more complex information required for more specific classification. For example, a single frame from a video of a swimmer doing breaststroke gives enough context for an image classifier to understand it is a swimmer in a swimming pool, but not enough to differentiate between swimming styles because it is the motion which differentiates the action. The architecture of the temporal stream, and how these two streams are finally fused is the subject of much research. Unfortunately, this approach does not transfer well to the radar domain. To function, optical flow makes a number of assumptions, once of which is that the pixel intensities of an object do not change between consecutive frames \cite{Bradski2008}. Since RDMs have only a single channel, which measures intensity, for which the values change frequently because of noise, this assumption does not hold. As such, the approach overall is not applicable.

State of the art approaches exist that do not use optical flow, such as \cite{Diba17}. In this case a novel transfer learning technique is used to train a deep 3-dimensional convolutional network from a pre-trained deep 2-dimensional image classification network. As mentioned earlier, it is likely that transfer learning from the optical domain does not work well for radar tasks. As such, this approach is also not feasible for this project, due to the resources required to train such deep networks from scratch.



\subsection{Autoencoders}
As previously discussed (Table \ref{table:DataSetReview}), there is an issue in this field of low sample support when training and evaluating machine learning approaches. Deep image classification networks tend to use very large datasets to avoid overfitting and improve generalisation. If only a small set of some niche image data is available, then it is common to use transfer learning from another image classification network to gain the benefits of a network trained on a larger dataset, which can then be tuned for purpose. However, as stated, similar datasets and pretrained networks are not available for the radar domain. This issue is directly addressed in \cite{Seyfioglu17}, and an approach for mitigating the problem through unsupervised pretraining via convolutional autoencoders is proposed.

\begin{figure}
	\centering
	\includegraphics[scale=0.7]{res/diagrams/autoencoder.png}
	\caption{Example of an autoencoder network \cite{AutoencoderExample}}
	\label{fig:autoencoder_example}
\end{figure}

Autoencoders are an unsupervised, or self-supervised, method of creating efficient, domain-specific data compression \cite{Goodfellow16} (Figure \ref{fig:autoencoder_example}). The input passes through a number of hidden layers which lower its dimensionality until it reaches the bottleneck, which is the point of lowest dimensionality. This is the encoder half of the approach. The compressed input is then passed to the decoder, which is often symmetrical to the encoder part, which decompresses the input until it outputs data of the same shape as the original input. The objective of this type of network is to have the input and output match. As such, the network learns to select the most important features from the input data to include in the compressed representation, so the highest fidelity output can be generated. It can be thought of as similar to principal component analysis, in that only the components which describe the most variance are kept.

This research proposed pretraining an autoencoder to learn an efficient representation of the data, before removing the decoder and adding a number of densely connected layers for fine-tuning and classification. This approach was compared against the same network structure, without any unsupervised pretrianing. The details of the dataset are shown in Table \ref{table:DataSetReview}. It was found that the pretrained approach was 95\% accurate, and outperformed random initialisation by 10\%. This shows that the performance of models trained with small datasets can be boosted using this approach.

An example of autoencoders being applied to RDMs is presented in \cite{Jokanovic17}, although it is not from the automotive domain. Instead it was in a healthcare context, where the goal was classifying gross human motion (walking, falling, sitting, and bending) using three different data representations as input to an artificial neural network. Time-frequency (spectrogram), range-map, and RDM data were input into separate paths, each using a stacked sparse autoencoder for feature extraction and softmax regression for classification. The final output classification was decided by unweighted voting between the three streams. The dataset used to train the models was very small, with only 94 samples across four classes (Table \ref{table:DataSetReview}). It was found that using joint input outperformed any single data input approach, and produced perfect accuracy for two of the classes, and 90\% or above for the others. The spectrogram and RDM alone produced average accuracies of 89.8\%, and 90.6\%, respectively. This shows that RDMs potentially can perform just as well as spectrograms in classification applications. However, the results are not particularly reliable because of the limited dataset.


\subsubsection{Denoising Autoencoders}
Radar data tends to be quite noisy, for a number of reasons, and there are many traditional methods for attempting to remove noise, one of which is CFAR. However, it is an interesting endeavour to attempt investigate if machine learning can be used to find new techniques. One popular method is denoising autoencoders (DAE). They are the same as the autoencoders explained above, however the objective of recreating the input as closely as possible is changed. During training, clean signal data has artificial noise added to it before it is input to the network. Now the goal of the autoencoder is to recreate the original clean signal, which is achieved by using the clean signal for comparison at the output \cite{Goodfellow16}. Variations on this approach have been shown to work for radar spectrograms of human motion \cite{Armanious18}. However, there is one major drawback, and that is the need for clean data from the outset. Since this is a supervised learning technique, the denoised output can only perform as well as whichever method has been chosen to clean the input originally.

Alternatively, a method has been proposed for denoising audio spectra without access to clean data, using partitioned autoencoders \cite{Stowell15}. This method uses a soft regularisation scheme to penalise latent variables in the bottleneck, such that a certain number learn to represent foreground signal, and the other background noise. Meaning, once training is complete, the background latents can be masked and the noise explicitly removed (Figure \ref{fig:pae_paper_fig}). This improves on the standard DAE approach, in that the network can learn its own representation of the noise, and therefore potentially outperform a traditional method. Regularisation is achieved in two steps. At the input, minibatches must be organised so that there are a known number of noise-only samples in each. These minibatch items are assigned a weak label $y$, which equals $1$ if the sample is noise-only, or $0$ otherwise. The second step is the use of a novel loss function:
\begin{equation}
l(X, y) = ||X - \hat{X}||^{2} + \frac{\lambda{y}}{\bar{C}}||C \odot f(X)||^{2},
\end{equation}
where $X$ is the input, $\hat{X}$ is the decoder output, $||\cdot||$ is the Frobenius norm, $\lambda$ is a regularisation coefficient, $C$ is a masking matrix that contains 1 for latents which should represent foreground and 0 otherwise, and $f(\cdot)$ is the encoder function. The first half of the loss function is similar to the standard AE objective; it compares the difference between the input and the output of the network. This drives the network to recreate the input as best as possible. The second half looks at the activation of the bottleneck latents ($f(X)$) and if those designated as foreground, by $C$, have activated where $y$ equals 1, then the loss is increased proportional to $\lambda$, therefore penalising the activation. The approach was tested using noisy audio spectra of bird calls with artificially added restaurant noise. This was so the results could be compared against a traditional DAE. The signal-to-noise ratio was compared before and after training, with the background latents masked, and it was found that the new approach outperformed the DAE. As expected the DAE had no incentive to overshoot the artificially added noise to remove any of the noise intrinsic to the bird call recordings. However, it was also found that the approach did not generalise well, and underperformed when new background recordings were used. This approach could have similar, or improved, success in the radar domain, as the background noise of radar is much less varied than acoustic noise.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.7, trim=0 40 0 0, clip=true]{res/diagrams/pae_figure_paper.jpg}
	\caption{Partitioned denoising autoencoder reconstruction after training \cite{Stowell15} (Figure 3)}
	\label{fig:pae_paper_fig}
\end{figure}



%\subsection{Supervised Learning}

%
%
% into three separate stacked autoencoders streams for feature extraction, before being fed into 


% review of all the papers! (critical of Practical Classification of... because of their dumb data)
% validation methods used in papers





%\paragraph{Multiple Joint-Variable Domains Recognition of Human Motion}
% cite Jokanovic17
%...



%\paragraph{Practical classification of different moving targets using automotive radar and deep neural networks}
%\cite{Angelov18}
% brings together numerous other research [XXX][XXX][XXX]
%...



%\subsubsection{Video Analysis}
%...
% justify that time analysis is a thing, so might be useful



% \subsection{Validation Metrics}
% ...
% is this needed?

%
%\subsection{Unsupervised Learning}
%...
% som, autoencoders (DAE + PAE)

% som -> intra-class variability (classification is inter-class)

%\subsubsection{Self-Organising Maps}
%...
% brief! can I just introduce this during implementation?

%\subsubsection{Autoencoders}
%...

% talk about low training sample support
% and then about PAE

%\section{Denoising}
%% non-ML methods do exist, but this paper is about pushing possibilities of ML
%...
%
%\section{Micro-Doppler Analysis}
%...
%
%\section{Automotive Approaches}
%...
%
%\section{Video Classification}


\pagebreak
