% --------------------------------------------------
% Design / Methodology
% --------------------------------------------------

% Here you should identify and evaluate the solution options against your problem statement / technical specification and make a reasoned choice of your chosen solution approach. Why did you do what you did? Conclude with a succinct definition of your solution approach and criteria by which the solution would be accepted as adequately verified. It is very likely that you will add further reference material in this section. 

\chapter{Solution Approach}
Given the problem space and objectives defined in \ref{section:Problem}, and the existing technology explored in Section \ref{chapter:Lit}, it is possible to outline potential solution designs.


% - identify and evaluate solution options against the problem statement / tech spec
% - make a reasoned choice of your chosen solution approach
% - why did you do what you did?
% - conclude with succinct definition of your approach and criteria by which the solution would be accepted as adequately verified
% - more references


% - identify possible approaches, high level (deferred / real time architecture decisions)
% - pick one and justify
% - high level flow chart
% - split into modules and discuss possible approaches and pick one from each
% - at each level define appropriate testing metrics
% - finally bring together for final approach overview


\section{Data Set} \label{section:DataSetApproach}

% TODO: talk about video capture! -- talk about what data will actually BE COLLECTED! (easier to do after radar prereqs section)
% TODO: talk about data storage? back ups required etc?
% TODO: justify radar parameters

As shown in Section \ref{chapter:Lit}, no public data set exists that fulfils the requirements laid out in \ref{section:DataReqs}, meaning a bespoke data set must be produced. While this will require a significant time investment, it provides great flexibility regarding the specification of the data set, as well as forming another useful deliverable for the stakeholders.

The range of road traffic types is vast, and can be categorised in any number of ways depending on specificity. For example, categorising by specific vehicle models would produce very different results compared to simple high-level labels. Highly specific categories would prove exceptionally useful in infrastructure use cases, as accurate nation-wide metrics on the models of vehicles on the road would be very valuable to automotive companies. However, as found in the review, existing studies generally use high-level labels, which suggests that radar alone is not suitable for this kind of detailed classification. Additionally, given the resources available to this project it is unlikely that a wide range of vehicles would be available for capture under controlled circumstances. The possibility of collecting data on the University of Reading internal road network was considered with the aim of increasing the range of vehicles available. However, this would only be ethical if consent were recorded for all participants, which would be impractical. Doing this would have provided realistic data, making the set more representative of the actual use-case, but it would also have made the data difficult to process into separate target samples. Attempting to classify multi-class multi-samples is a possible approach, but it was decided against because the majority of automotive research used single-class single-target samples, and the aim here was to recreate their success. Additionally, the risk associated with multiple vehicles and pedestrians in close proximity would be too great, which once again makes this solution impractical. For these reasons, the following high-level class labels were selected: No-Target, One Pedestrian, Two Pedestrians, Bicycle, and Car; as used in \cite{Angelov18}. These are somewhat limited, as they do not include common types of large vehicle, such as buses or LGVs, which are of interest to the DfT \cite{DfT18}. The "No-Target" class was chosen in order to provide a clear noise baseline, and to investigate whether noise-only samples can be used by models to differentiate signal and noise effectively. The "Two Pedestrians" class was chosen as a compromise for the inclusion of multi-target samples. The aim is that it will indicate not only the separability of pedestrian groups, but groups of other road traffic as well.

To improve the generality of this data set, it is important to consider more than just the target classes. This data is attempting to represent scenarios such as a vehicle or pedestrian travelling down a straight road, or crossing a junction. A traffic management radar could be placed at any number of points within such a scene, meaning objects will be detected at a variety of ranges, speeds, and incident angles to the radar. As found in \ref{section:uDAnalysis}, angle of approach can significantly affect the signature of a target, and it is likely that range will also have an effect as increased distance will reduce reflected signal power. To ensure this variance is captured, a number of scenarios should be defined which vary these variables in a controlled way. Part of achieving this goal will be having a large enough data set. As shown in \ref{table:DataSetReview}, some similar studies use only hundreds of samples whereas others use upwards of 1000. As discussed in \ref{section:ExistingDataSets}, 650 samples should be seen as a minimum. As such, the aim is to create a data set of at least 1000 samples in total, so the proposed minimum size is surpassed, and to remain comparable with current research.

As mentioned in the requirements specification, the data should be collected using a 77GHz FMCW radar. An elevation of 1.8m will be used, as this satisfies the technical height requirement and is sufficiently different from the automotive research that was reviewed. Most of the existing research focuses on analysing $\mu$-D features with spectrograms, so instead RDMs will be used here. To allow temporal analysis, multiple RDM frames will be included in each sample. A duration of 2 seconds will be used for each sample, because although 2.5$s$ gave the best results, the use of longer samples means fewer can be generated from a given data collection. This can especially impact the number of samples available for fast moving objects, such as cars, as they will exit the sensor range before enough samples can be collected. Using a 2$s$ duration balances higher classification accuracy and capturing more samples in practice. Additionally, as pedestrians are a target class, walking cadence must be taken into account. Average adult walking cadence is approximately 1.7 steps/second \cite{Tudor18}, meaning a 2$s$ sample will capture almost 4 steps, which should be more than sufficient for classification. To be able to ground-truth the data, video will be collected alongside each sample. Metadata in accordance with the requirement specification will also be generated.


\section{System Architecture} \label{section:SystemArchitecture}
As the overarching objective of this project is to provide classification in an infrastructure scenario, it is important that its application to real-world use-cases is prioritised. As discussed in \ref{section:ExistingProducts}, existing products produce real-time object tracking and classification output. This is critical for remaining applicable to their use-case, as traffic management is a real-time problem, one which cannot be deferred for processing and resolution after-the-fact. Although the goal here is not to produce a feature-complete, deployable system, it could be argued that a real-time system should be implemented to maintain relevance. This would potentially give a more realistic representation of end-to-end performance, and thus allow better evaluation of the system's usefulness. However, as mentioned in \ref{section:Constraints}, only consumer hardware is available, meaning performance metrics related to processing time would not be indicative of results obtained on dedicated hardware such digital signal processors (DSPs) or SoCs, and so would only be useful for relative comparison. In addition, the focus of this project is on investigating methods of classification, not real-time processing. The problem of efficient, real-time radar data processing is out of scope, and any incidental research and development here would provide little benefit to industry stakeholders who already have their own mature systems in place.

Instead, a deferred system is proposed. The ability to batch process input and store intermediate output encourages modularity and allows for a faster development cycle, as the entire end-to-end system would not need to be run every time a small change is made. However, to maintain relevance to the use-case, it is important the system mimics the structure of real-time systems used in industry. Using examples from AGD Systems Ltd., and taking the structure presented in \cite{Angelov18} as a foundation, the pipeline design shown in \ref{fig:pipeline} was produced. The diagram uses yellow highlighting for the real-time inspired modules, and blue for those support batch processing.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.5]{res/high-level-pipeline.jpg}
	\caption{Proposed pipeline architecture}
	\label{fig:pipeline}
\end{figure}

The pipeline logically separates raw data processing, tracking, and classification, allowing them to be implemented independently. This means input and output can be managed in a batch-wise manner, while internal processes can perform unit-wise, as a real-time system would. Quality metrics, as discussed in Section \ref{chapter:Lit}, are produced where appropriate and the modular design provides maintainability, fulfilling those technical requirements.

Each of these modules will require their own user interface. Graphical interfaces provide great usability and a low barrier to entry in terms of technical knowledge. However, as defined in \ref{section:Stakeholders}, the output of this project is not intended for non-technical users, meaning ease-of-use is not as important. Due to the additional development time this would require, graphical interfaces are deemed to be outside of the project scope. Instead, a command-line interface (CLI) will be used. This provides increased flexibility compared to a graphical interface, whilst having a lower development cost.


\section{Classification Methods Investigation} \label{section:ClassificationApproach}

% TODO: name sepcific models that are going to be used in the first testing pass?
% TODO: generally be more specific?
% TODO: mention that it would be good to find solutions for both stills and temportal data, so it is applicable in different use cases?

% TODO: name specific models that are used in the second model phase? dont include proper justification?, just x was successful so y

As the majority of existing literature in the automotive space has seen success using artificial neural networks (ANNs) for classification, a similar approach will be taken here. The hypothesis being tested is whether ANNs can classify road traffic using radar data collected in an infrastructure scenario, formatted as a RDM. If shown to be possible, the objective is to compare existing methods in the automotive space and see if their success transfers to this domain.

Before the experimentation phase, data exploration will occur. This will be to discover information about the distribution and shape of the data; the existence of outliers; if classes can be differentiated by the human eye; and what clusters might exist within the data beyond the defined class labels (intra-class vs inter-class). Some of this process has been recorded in Section \ref{chapter:Data}. This information will then be used to determine valid preprocessing steps.

After this, experimentation will begin and models will be considered. To facilitate this process a modular model training manager will be created, allowing models to be prototyped quickly and executed in a generic way, avoiding the need for repeating boilerplate code. This will have the added benefit of ensuring the implementation remains maintainable and supports future research by the stakeholders, as required by the specification. This is shown in \ref{fig:pipeline} as the "Model Trainer". A number of ANN architectures have been reviewed in \ref{section:ClassificationMethodsReview} and these will be used as the basis for models created here, allowing the direct comparison of spectrogram and RDM input formats. Both single frames and the time-series samples will be considered as input for these models, meaning the effect of including temporal information can be analysed. Video classification techniques will not be investigated as part of this project. As shown in \ref{section:ClassificationMethodsReview}, the state-of-the-art approach to video classification requires techniques that do not transfer well to RDMs, so developing alternative methods falls outside the scope of this project.

%These models will produce a baseline set of results, and provide an answer to the first hypothesis. Next, more complex models will be considered and developed in order to test the second objective. This tiered approach is being used instead of a single pass because it will allow learnings from the first hypothesis result to be taken into the next. This is important as high classification accuracy is a key concern of the stakeholders.

To ensure the reliability of any results, repeat validation will be required. As the dataset will not be very large, a simple approach such as the holdout method will not be enough. Nested k-fold cross-validation will be employed to ensure there is no participant information leakage between train and test sets, whilst allowing the entirety of the dataset's variance to be exposed to the model, so more reliable performance and generalisation error values can be calculated. There are many useful measures of performance, the most common being accuracy. However, when applied to imbalanced sets, accuracy is biased towards classes with more instances. This can be managed through rebalancing and leaving out samples, and is used in \cite{Angelov18}, but as this data set will be small, removing samples is not a good solution. Instead, metrics such as precision, recall, F-measure, and AUC-ROC should be used, as these use confusion matrix values to produce less biased results. As ANNs are the primary method being used, loss will also be an important metric, because it describes the error between the prediction output and the true value. Using these metrics, the two objectives can be verified.






% how am I going about investigating which classification method is best?
% start with how machine learning is probably the best route, given the existing studies

% outline the logical method I am using
% starting point -> hyopthesis, methods, how measure results, how decide is acceptable?
% define baseline methods
% review outcomes
% generate more complex approach
% review
% stills vs video
% exploratory analysis
% creation of a cradle for all training to occur within (diagram)
% justify why I am doing it this way -> use lit
% managing with a small data set, considerations?
% validation methods - nested cross fold, use of data subsets
% 
% - outline different machine learning methods
%
%- link to data section and mention that its worth investigating if within class classification is possible (although it is outside scope kinda)


\section{Software Engineering Approach}

To implement this project in good time, to a standard that will satisfy the requirements and stakeholders, a good software engineering approach should be used. As there is only one developer for this project, and no manager, and because research will continue to be conducted throughout the process, an agile approach will be taken. "Agile" is an iterative workflow, which moves through phases of design, develop, test, and review. This means development can be flexible and reactive, allowing new research to be incorporated into the project as it is found. There will be frequent meetings with stakeholders as part of the review process, the feedback from which will inform the next phase of design decisions. Design goals and progress will be recorded using a Kanban board and the project log book. A version control system, namely git via gitlab, will be used to record fine detail about the implementation process. The Kanban board will integrate with the version control, so progress will be captured synchronously.


\section{Solution Overview}
% - how verified? (each section will produce metrics?)

The data set will contain over 1000 samples, across 5 target classes: No-Target, One Pedestrian, Two Pedestrians, Bicycle, and Car. Each sample will have a duration of 2 seconds and be in the form of a RDM. The range, speed, and angle of approach will be varied for each target class so the dataset is diverse. The size and variability of the dataset will be used to determine if this approach was successful.

The system for processing samples and predicting class labels will take the form of a pipeline. It will be designed to support batch processing, but at each step the core processes will attempt to perform in a real-time manner, with the goal simulating the use-case more closely. Each module will use a command-line interface, as a graphical interface is outside of the project scope. Metrics will be produced where appropriate, such as from the tracking and classification modules, and will be used to determine if this approach was successful.

The investigation of classification methods will take an experimental approach. Exploratory analysis will take place initially, to discover the shape of the data. A set of models will be implemented in similar ways to those reviewed in \ref{section:ClassificationMethodsReview}. Results will used to optimise model parameters and structure. Metrics, such as F-measure, will be used to compare models and determine if traffic can be classified using RDMs and if classification was successful enough to pass acceptance testing.

%Development will be an agile process, with sprints being used to manage development time. There will be frequent contact with stakeholders to keep the project on track, and to act as part of the evaluation and planning step of the agile cycle. A logbook will be kept containing relevant notes and thoughts about the project.

\pagebreak
